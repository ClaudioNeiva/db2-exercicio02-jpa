package br.ucsal.bes20172.bd2.exercicio02jpa.enums;

public enum SituacaoFuncionarioEnum {

	ATIVO("A"), DESLIGADO("D");

	private String cod;

	private SituacaoFuncionarioEnum(String cod) {
		this.cod = cod;
	}

	public String getCod() {
		return cod;
	}

	public static SituacaoFuncionarioEnum valueOfCodigo(String cod) {
		for (SituacaoFuncionarioEnum situacaoFuncionarioEnum : values()) {
			if (situacaoFuncionarioEnum.cod.equalsIgnoreCase(cod)) {
				return situacaoFuncionarioEnum;
			}
		}
		throw new IllegalArgumentException();
	}

}
