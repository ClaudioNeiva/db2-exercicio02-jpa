package br.ucsal.bes20172.bd2.exercicio02jpa.exemplo;

import java.lang.reflect.Field;

import br.ucsal.bes20172.bd2.exercicio02jpa.domain.Departamento;

public class ReflexaoExemplo {

	public static void main(String[] args)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Departamento departamento = new Departamento();
		Class<Departamento> clazz = Departamento.class;
		Field[] campos = clazz.getFields();
		for (Field campo : campos) {
			System.out.println(campo.getName());
		}
		Field codField = clazz.getDeclaredField("cod");// getDeclaredField
														// procura
														// campos privates
														// tamb�m
														// mas apenas na classe
														// n�o
														// procura na
														// hierarquia.
		codField.setAccessible(true);
		codField.set(departamento, "TESTE1");

		System.out.println(codField.get(departamento));
		System.out.println(departamento.getCod());
	}

}
