package br.ucsal.bes20172.bd2.exercicio02jpa.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Departamento {

	@Id
	private String cod;

	private String nome;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "departamento")
	private List<Funcionario> funcionarios;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "departamento")
	private List<Loja> lojas;

	public Departamento() {
	}

	public Departamento(String cod, String nome) {
		super();
		this.cod = cod;
		this.nome = nome;
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}

	@Override
	public String toString() {
		return "Departamento [cod=" + cod + ", nome=" + nome + ", funcionarios=" + funcionarios + "]";
	}
}
