package br.ucsal.bes20172.bd2.exercicio02jpa.exemplo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.ucsal.bes20172.bd2.exercicio02jpa.domain.Funcionario;
import br.ucsal.bes20172.bd2.exercicio02jpa.enums.SituacaoFuncionarioEnum;

public class FuncionarioExemplo {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("exercicio02");
		EntityManager em = emf.createEntityManager();
		try {

			Funcionario funcionario1 = em.find(Funcionario.class, 2);

			System.out.println("funcionario1=" + funcionario1);

			Funcionario funcionario2 = new Funcionario("Manuela", SituacaoFuncionarioEnum.ATIVO);

			em.getTransaction().begin();
			em.persist(funcionario2);
			em.getTransaction().commit();

		} finally {

			if (emf != null) {
				emf.close();
			}

		}
	}

}
