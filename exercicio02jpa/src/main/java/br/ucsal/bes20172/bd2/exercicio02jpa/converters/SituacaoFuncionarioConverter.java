package br.ucsal.bes20172.bd2.exercicio02jpa.converters;

import javax.persistence.AttributeConverter;

import br.ucsal.bes20172.bd2.exercicio02jpa.enums.SituacaoFuncionarioEnum;

public class SituacaoFuncionarioConverter implements AttributeConverter<SituacaoFuncionarioEnum, String> {

	@Override
	public String convertToDatabaseColumn(SituacaoFuncionarioEnum situacaoFuncionarioEnum) {
		return situacaoFuncionarioEnum.getCod();
		// if (SituacaoFuncionarioEnum.ATIVO.equals(situacaoFuncionarioEnum)) {
		// return "A";
		// }
		// return "D";
	}

	@Override
	public SituacaoFuncionarioEnum convertToEntityAttribute(String valor) {
		return SituacaoFuncionarioEnum.valueOfCodigo(valor);
		// if (valor.equals("A")) {
		// return SituacaoFuncionarioEnum.ATIVO;
		// }
		// return SituacaoFuncionarioEnum.DESLIGADO;
	}

}
