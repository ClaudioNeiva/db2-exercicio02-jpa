package br.ucsal.bes20172.bd2.exercicio02jpa.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Loja {

	@Id
	private Integer id;

	private String nome;

	@ManyToOne
	@JoinColumn(name = "cod_departamento")
	private Departamento departamento;

}
