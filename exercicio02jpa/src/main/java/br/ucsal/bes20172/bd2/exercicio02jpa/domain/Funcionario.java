package br.ucsal.bes20172.bd2.exercicio02jpa.domain;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.ucsal.bes20172.bd2.exercicio02jpa.converters.SituacaoFuncionarioConverter;
import br.ucsal.bes20172.bd2.exercicio02jpa.enums.SituacaoFuncionarioEnum;

@Entity(name = "tab_funcionario")
public class Funcionario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "func_id")
	private Integer id;

	@Column(name = "func_nm")
	private String nome;

	@Column(name = "func_st")
	@Convert(converter = SituacaoFuncionarioConverter.class)
	private SituacaoFuncionarioEnum situacao;

	@ManyToOne
	@JoinColumn(name = "cod_departamento")
	private Departamento departamento;

	public Funcionario() {
	}

	public Funcionario(String nome, SituacaoFuncionarioEnum situacao) {
		super();
		this.nome = nome;
		this.situacao = situacao;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public SituacaoFuncionarioEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoFuncionarioEnum situacao) {
		this.situacao = situacao;
	}

	@Override
	public String toString() {
		return "Funcionario [id=" + id + ", nome=" + nome + ", situacao=" + situacao + ", departamento="
				+ departamento.getCod() + "|" + departamento.getNome() + "]";
	}

}
