package br.ucsal.bes20172.bd2.exercicio02jpa.exemplo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.ucsal.bes20172.bd2.exercicio02jpa.domain.Departamento;

public class Exemplo01 {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("exercicio02");
		EntityManager em = emf.createEntityManager();
		try {

			Departamento departamento = em.find(Departamento.class, "VND");

			System.out.println(departamento);

			em.getTransaction().begin();
			departamento.setNome("Pera");
			em.getTransaction().commit();

			Departamento departamentoNovo = new Departamento("INF", "Novo2");

			em.getTransaction().begin();
			em.persist(departamentoNovo);
			em.getTransaction().commit();

			em.getTransaction().begin();
			departamentoNovo.setNome("Informática");
			em.getTransaction().commit();

		} finally {

			if (emf != null) {
				emf.close();
			}

		}
	}

}
