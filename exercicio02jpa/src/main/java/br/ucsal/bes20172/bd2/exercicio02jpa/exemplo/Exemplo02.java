package br.ucsal.bes20172.bd2.exercicio02jpa.exemplo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.sun.jna.platform.win32.OaIdl.DECIMAL;

import br.ucsal.bes20172.bd2.exercicio02jpa.domain.Departamento;
import br.ucsal.bes20172.bd2.exercicio02jpa.domain.Funcionario;

public class Exemplo02 {
	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("exercicio02");
		EntityManager em = emf.createEntityManager();
		try {

			// Funcionario funcionario = em.find(Funcionario.class, 5);
			//
			// System.out.println(funcionario);

			Departamento departamento = em.find(Departamento.class, "INF");

			System.out.println(departamento.getNome());

		} finally {

			if (emf != null) {
				emf.close();
			}

		}
	}

}
